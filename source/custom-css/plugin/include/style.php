<?php
/**
 * Copyright (C) 2018-2022 Ethan P. (eth-p)
 */

$PLUGIN     = explode('/', $_SERVER['PHP_SELF'])[2];
$FILE       = $_GET["file"] ?? "unraid.css";
$STYLESHEET = '/boot/config/plugins/' . $PLUGIN . '/' . $FILE;

header('Content-Type: text/css');

if (file_exists($STYLESHEET)) {
	$fh = fopen($STYLESHEET, 'r');
	fpassthru($fh);
	fclose($fh);
}

